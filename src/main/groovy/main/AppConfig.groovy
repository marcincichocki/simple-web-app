package main

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Created by user on 30/12/2015.
 */
@Configuration
class AppConfig {

    @Value('${message}')
    String message

    @Bean
    Message message(){
        new Message(message: message)
    }
}
