package main

import groovy.json.JsonOutput
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

/**
 * Created by user on 30/12/2015.
 */
@RestController
class SimpleController {

    @Autowired
    Message message

    @RequestMapping(value = '/hello/{param}', method = RequestMethod.GET, produces = 'application/json; charset=UTF-8')
    String index(@PathVariable String param) {
        message.param = param + 'test'
        JsonOutput.toJson(message)
    }
}
